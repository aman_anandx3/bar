package vgroup.com.scanngen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import vgroup.com.scanngen.base.AppConstants;


public class SplashActivity extends BaseActivity {

    private Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_splash);
        activity = SplashActivity.this;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (activity == null) {
                    return;
                }
                if (Utils.readSharedSettingBoolean(activity.getApplicationContext(), AppConstants.ISINTROSHOW, false)) {
                    Intent myProfileIntent = new Intent(activity, HomeActivity.class);
                    startActivity(myProfileIntent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                } else {
                    Intent myProfileIntent = new Intent(activity, IntroActivity.class);
                    startActivity(myProfileIntent);
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    finish();
                }

            }
        }, 2000);

    }

    @Override
    public void connected() {

    }

    @Override
    public void disconnected() {

    }
}
