package vgroup.com.scanngen.intro;


import android.os.Bundle;
import vgroup.com.scanngen.R;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import vgroup.com.scanngen.Utils;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link IntroFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class IntroFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String TITLE = "title";
    private static final String DESC = "desc";
    private static final String IMAGE = "image";
    private static final String BG_COLOR = "bg_color";


    // TODO: Rename and change types of parameters
    private String title;
    private String desc;
    private int image, bg_color;


    public IntroFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param title       Parameter 1.
     * @param description Parameter 2.
     * @return A new instance of fragment BlankFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static IntroFragment newInstance(String title, String description, int drawable, int bgColor) {
        IntroFragment fragment = new IntroFragment();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(DESC, description);
        args.putInt(IMAGE, drawable);
        args.putInt(BG_COLOR, bgColor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            title = getArguments().getString(TITLE);
            desc = getArguments().getString(DESC);
            image = getArguments().getInt(IMAGE);
            bg_color = getArguments().getInt(BG_COLOR);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v;
        TextView titleTV, descTV;
        ImageView imageView;
        if (Utils.isTablet()) {
            v = inflater.inflate(R.layout.intro_item_tab, container, false);
        } else {
            v = inflater.inflate(R.layout.intro_item, container, false);
        }
        titleTV = v.findViewById(R.id.title);
        descTV = v.findViewById(R.id.description);
        imageView = v.findViewById(R.id.image);
        titleTV.setText(title);
        descTV.setText(desc);
        imageView.setImageResource(image);
        v.getRootView().setBackgroundColor(bg_color);


        return v;

    }

}
