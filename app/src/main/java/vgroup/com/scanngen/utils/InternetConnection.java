package vgroup.com.scanngen.utils;

/**
 * Created by DELL on 1/19/2018.
 */

public interface InternetConnection {
    void connected();

    void disconnected();
}
