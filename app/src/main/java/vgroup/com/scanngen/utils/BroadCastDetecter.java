package vgroup.com.scanngen.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import vgroup.com.scanngen.base.App;

/**
 * Created by DELL on 1/19/2018.
 */
public class BroadCastDetecter extends BroadcastReceiver {
    public static InternetConnection internetConnection;
    public static boolean internet_status = false;
    private static Context context;

    public static void checkInternetConenction(Context context) {
        internet_status = false;
        ConnectivityManager check = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (check != null) {
            NetworkInfo[] info = check.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        internet_status = true;

                    }
                    if (info[i].getState() == NetworkInfo.State.DISCONNECTED) {
                        internet_status = false;
                    }
                    if (internet_status) {

                        break;
                    }
                }
            App.setConnected(internet_status);
            if (internet_status) {
                internetConnection = (InternetConnection) getContext();

                internetConnection.connected();

                //do what you want to if internet connection is available
            } else {
                internetConnection = (InternetConnection) getContext();
                internetConnection.disconnected();

            }
        }
    }

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context1) {
        context = context1;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals("android.net.conn.CONNECTIVITY_CHANGE")) {
            try {
                checkInternetConenction(context);
            } catch (Exception e) {

            }
        }
    }
}