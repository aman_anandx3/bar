package vgroup.com.scanngen.utils;

import android.util.Log;
import android.view.MotionEvent;

/**
 * Created by DELL on 1/3/2018.
 */

public class GestureDetector implements android.view.GestureDetector.OnGestureListener {
    SwipeListener swipeListener;

    public GestureDetector(SwipeListener swipeListener) {
        this.swipeListener = swipeListener;
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        Log.i("motion", "onFling has been called!");
        final int SWIPE_MIN_DISTANCE = 120;
        final int SWIPE_MAX_OFF_PATH = 250;
        final int SWIPE_THRESHOLD_VELOCITY = 200;
        try {
            if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                return false;
            if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                Log.i("motion", "Right to Left");
                swipeListener.OnSwipeLeft();
//                Utils.getInstance().switchTabs(false,tabHost);
            } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                    && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                Log.i("motion", "Left to Right");
//                Utils.getInstance().switchTabs(true,tabHost);
                swipeListener.OnSwipeRight();

            }
        } catch (Exception e) {
            // nothing
        }
        return false;

    }

}
