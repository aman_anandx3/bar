package vgroup.com.scanngen;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import io.realm.Realm;
import io.realm.RealmResults;
import vgroup.com.scanngen.adapter.BarcodeAdapter;
import vgroup.com.scanngen.adapter.NfcAdapter;
import vgroup.com.scanngen.adapter.OcrAdapter;
import vgroup.com.scanngen.base.AppConstants;
import vgroup.com.scanngen.base.bean.BarcodeBean;
import vgroup.com.scanngen.base.bean.NfcBean;
import vgroup.com.scanngen.base.bean.OcrBean;

public class ListActivity extends BaseActivity implements PopupMenu.OnMenuItemClickListener {
    private GestureDetector mGestureDetector;
    private ImageView backBtn;
    private View indi_scanned, indi_generated, indi_nfc, indi_ocr;
    private TextView empty;
    private int currentTab = 0;
    private ProgressBar progressBar;
    private LinearLayout scanned_lay, generated_lay, nfc_lay, ocr_lay;
    private boolean empty_scanned = false, empty_generated = false, empty_nfc = false, empty_ocr = false;
    private AdView mAdView;
    private RelativeLayout rootLay;
    private Realm realm;
    private RecyclerView recyclerView;
    private Menu menu;
    private BarcodeAdapter adapter, adapterGenerated;
    private OcrAdapter ocrAdapter;
    private NfcAdapter nfcAdapter;
    private ImageButton optionBtn;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Utils.isTablet()) {
            setContentView(R.layout.activity_list_tab);
        } else {
            setContentView(R.layout.activity_list);
        }
        realm = Realm.getDefaultInstance();
        setupData();
        init();
        MobileAds.initialize(this, getString(R.string.admob_app_id));
        mAdView = findViewById(R.id.adView);
        mAdView.setVisibility(View.GONE);

        scanned_lay.performClick();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (adapter != null && currentTab == 1 && adapter.getItemCount() == 0) {
            show(empty);
            hide(recyclerView);
        } else if (ocrAdapter != null && currentTab == 2 && ocrAdapter.getItemCount() == 0) {
            show(empty);
            hide(recyclerView);
        } else if (nfcAdapter != null && currentTab == 3 && nfcAdapter.getItemCount() == 0) {
            show(empty);
            hide(recyclerView);
        } else if (adapterGenerated != null && currentTab == 4 && adapterGenerated.getItemCount() == 0) {
            show(empty);
            hide(recyclerView);
        }
        loadAds(mAdView);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null);
        realm.close();
    }

    private void setupData() {
        RealmResults realmResults = realm.where(BarcodeBean.class).equalTo("isScanned", true).findAll();
        if (realmResults.size() > 0) {
//            adapter = new BarcodeAdapter(realm.where(BarcodeBean.class).isEmpty("image.name").findAll());
            adapter = new BarcodeAdapter(realm.where(BarcodeBean.class).equalTo("isScanned", true).findAll());
            empty_scanned = false;
        } else {
            empty_scanned = true;
        }
        realmResults = realm.where(BarcodeBean.class).equalTo("isScanned", false).findAll();
        if (realmResults.size() > 0) {
            adapterGenerated = new BarcodeAdapter(realm.where(BarcodeBean.class).equalTo("isScanned", false).findAll());
            empty_generated = false;
        } else {
            empty_generated = true;
        }
        realmResults = realm.where(OcrBean.class).findAll();
        if (realmResults.size() > 0) {
            ocrAdapter = new OcrAdapter(realm.where(OcrBean.class).findAll());
            empty_ocr = false;
        } else {
            empty_ocr = true;
        }
        realmResults = realm.where(NfcBean.class).findAll();
        if (realmResults.size() > 0) {
            nfcAdapter = new NfcAdapter(realm.where(NfcBean.class).findAll());
            empty_nfc = false;
//            empty.setVisibility(View.GONE);
//            recyclerView.setVisibility(View.VISIBLE);
        } else {
//            empty.setVisibility(View.VISIBLE);
//            recyclerView.setVisibility(View.GONE);
            empty_nfc = true;
        }
    }

    private void init() {
        context = this;
        scanned_lay = findViewById(R.id.scannedMainLayout);
        generated_lay = findViewById(R.id.generatedMainLayout);
        nfc_lay = findViewById(R.id.nfcMainLayout);
        ocr_lay = findViewById(R.id.ocrMainLayout);
        optionBtn = findViewById(R.id.options);
        indi_scanned = findViewById(R.id.scannedView);
        indi_generated = findViewById(R.id.generatedView);
        indi_nfc = findViewById(R.id.nfcView);
        indi_ocr = findViewById(R.id.ocrView);
        backBtn = findViewById(R.id.backImg);

        rootLay = findViewById(R.id.root);
        empty = findViewById(R.id.emptyText);
        recyclerView = findViewById(R.id.recyclerView);
        //setup Adapters
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
//        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        mGestureDetector = new GestureDetector(this, new GestureDetector.OnGestureListener() {

            @Override
            public boolean onDown(MotionEvent e) {
                return true;
            }

            @Override
            public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
                                   float velocityY) {
                Log.i("motion", "onFling has been called!");
                final int SWIPE_MIN_DISTANCE = 120;
                final int SWIPE_MAX_OFF_PATH = 250;
                final int SWIPE_THRESHOLD_VELOCITY = 200;
                try {
                    if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH)
                        return false;
                    if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                            && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                        Log.i("motion", "Right to Left");
//                        Utils.getInstance().switchTabs(false,tabHost);
                    } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                            && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {

                        Log.i("motion", "Left to Right");
//                        Utils.getInstance().switchTabs(true,tabHost);

                    }
                } catch (Exception e) {
                    // nothing
                }
                return false;
            }

            @Override
            public void onLongPress(MotionEvent e) {

            }

            @Override
            public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                    float distanceX, float distanceY) {
                return false;
            }

            @Override
            public void onShowPress(MotionEvent e) {

            }

            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return false;
            }
        });
        recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return mGestureDetector.onTouchEvent(event);
            }
        });

        scanned_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUi(AppConstants.BARCODE);
                if (empty_scanned) {
                    hide(recyclerView);
                    show(empty);
                } else {
                    hide(empty);
                    show(recyclerView);
                    recyclerView.setAdapter(adapter);
                }

            }
        });
        generated_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUi(AppConstants.GENERATED);
                if (empty_generated) {
                    hide(recyclerView);
                    show(empty);
                } else {
                    hide(empty);
                    show(recyclerView);
                    recyclerView.setAdapter(adapterGenerated);
                }
            }
        });
        nfc_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUi(AppConstants.NFC);
                if (empty_nfc) {
                    hide(recyclerView);
                    show(empty);
                } else {
                    hide(empty);
                    show(recyclerView);
                    recyclerView.setAdapter(nfcAdapter);
                }
            }
        });
        ocr_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeUi(AppConstants.OCR);
                if (empty_ocr) {
                    hide(recyclerView);
                    show(empty);
                } else {
                    hide(empty);
                    show(recyclerView);
                    recyclerView.setAdapter(ocrAdapter);
                }
            }
        });

        backBtn = findViewById(R.id.backImg);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        progressBar = findViewById(R.id.progressBar);

        optionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(optionBtn);
            }
        });

    }

    /**
     * Used to show dialog on UI.
     * @param v
     */
    private void showPopup(View v) {
        PopupMenu popup = new PopupMenu(context, v);

        // This activity implements OnMenuItemClickListener
        popup.setOnMenuItemClickListener(this);
        popup.inflate(R.menu.menu01);
        popup.show();
    }

    /**
     * Click listener for options menu.
     * @param item
     * @return
     */
    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.rateUs:
                rateUs();
                return true;
            case R.id.share:
                share();
                return true;
            case R.id.feedback:
                feedback();
                return true;
            default:
                return false;
        }
    }

    public void changeUi(String option) {
        switch (option) {
            case AppConstants.BARCODE:
                currentTab = 1;
                indi_generated.setBackgroundResource(R.color.colorPrimary);
                indi_nfc.setBackgroundResource(R.color.colorPrimary);
                indi_ocr.setBackgroundResource(R.color.colorPrimary);
                indi_scanned.setBackgroundResource(R.color.colorWhite);
                break;
            case AppConstants.OCR:
                currentTab = 2;
                indi_scanned.setBackgroundResource(R.color.colorPrimary);
                indi_generated.setBackgroundResource(R.color.colorPrimary);
                indi_nfc.setBackgroundResource(R.color.colorPrimary);
                indi_ocr.setBackgroundResource(R.color.colorWhite);

                break;
            case AppConstants.NFC:
                currentTab = 3;
                indi_scanned.setBackgroundResource(R.color.colorPrimary);
                indi_generated.setBackgroundResource(R.color.colorPrimary);
                indi_ocr.setBackgroundResource(R.color.colorPrimary);
                indi_nfc.setBackgroundResource(R.color.colorWhite);

                break;
            case AppConstants.GENERATED:
                currentTab = 4;
                indi_scanned.setBackgroundResource(R.color.colorPrimary);
                indi_nfc.setBackgroundResource(R.color.colorPrimary);
                indi_ocr.setBackgroundResource(R.color.colorPrimary);
                indi_generated.setBackgroundResource(R.color.colorWhite);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void connected() {
        loadAds(mAdView);
    }

    @Override
    public void disconnected() {
//        mAdView.setVisibility(View.GONE);
    }
}
