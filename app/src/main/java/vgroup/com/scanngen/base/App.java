package vgroup.com.scanngen.base;

import android.app.Activity;
import android.app.Application;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import io.realm.Realm;
import vgroup.com.scanngen.HomeActivity;
import vgroup.com.scanngen.utils.BroadCastDetecter;

/**
 * Created by DELL on 1/10/2018.
 */

public class App extends Application implements Application.ActivityLifecycleCallbacks {
    private static boolean isConnected = false;
    private static App mInstance;
    final String SOME_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";
    final IntentFilter intentFilter = new IntentFilter(SOME_ACTION);
    private BroadCastDetecter broadCastDetecter = new BroadCastDetecter();

    public static synchronized App getInstance() {
        return mInstance;
    }

    public static boolean isConnected() {
        return isConnected;
    }

    public static void setConnected(boolean connected) {
        isConnected = connected;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
        mInstance = this;
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
        BroadCastDetecter.setContext(activity);
        if (activity instanceof HomeActivity) {
            activity.registerReceiver(broadCastDetecter, intentFilter);
            Log.e("BReceiver", activity.getClass().getSimpleName() + " registered");
        }
    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {
        if (activity instanceof HomeActivity) {
            activity.unregisterReceiver(broadCastDetecter);
            Log.e("BReceiver", activity.getClass().getSimpleName() + " unregistered");
        }
    }
}
