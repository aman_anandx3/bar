package vgroup.com.scanngen.base;

/**
 * Created by DELL on 1/10/2018.
 */

public class AppConstants {
    public static final String BARCODE = "barcode";
    public static final String GENERATED = "generated";
    public static final String OCR = "ocr";
    public static final String NFC = "nfc";
    public static final String TYPE = "type";
    public static final String UPDATE = "update";
    public static final String ID = "id";
    public static final String PREFERENCES_FILE = "my_app_settings";
    public static final String ISINTROSHOW = "ISINTROSHOW";

}
