package vgroup.com.scanngen;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Window;
import android.view.WindowManager;

import com.github.paolorotolo.appintro.AppIntro;

import vgroup.com.scanngen.base.AppConstants;
import vgroup.com.scanngen.intro.IntroFragment;


public class IntroActivity extends AppIntro {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

//        addSlides();
        IntroFragment page1 = IntroFragment.newInstance(getString(R.string.introHeader_1), getString(R.string.introDesc_1), R.drawable.ic_launcher, getResources().getColor(R.color.intro_1));
        IntroFragment page2 = IntroFragment.newInstance(getString(R.string.introHeader_2), getString(R.string.introDesc_2), R.drawable.reader_icon, getResources().getColor(R.color.intro_2));
        IntroFragment page3 = IntroFragment.newInstance(getString(R.string.introHeader_3), getString(R.string.introDesc_3), R.drawable.generator_icon, getResources().getColor(R.color.intro_3));
        IntroFragment page4 = IntroFragment.newInstance(getString(R.string.introHeader_4), getString(R.string.introDesc_4), R.drawable.nfc_icon, getResources().getColor(R.color.intro_4));
        IntroFragment page5 = IntroFragment.newInstance(getString(R.string.introHeader_5), getString(R.string.introDesc_5), R.drawable.ocr_icon, getResources().getColor(R.color.intro_5));
        addSlide(page1);
        addSlide(page2);
        addSlide(page3);
        addSlide(page4);
        addSlide(page5);
        setSwipeLock(false);
        // OPTIONAL METHODS
        // Override bar/separator color.
        setBarColor(Color.parseColor("#00000000"));
//    setBarColor(Color.parseColor("#3F51B5"));
        setSeparatorColor(Color.parseColor("#FFFFFF"));


        // Hide Skip/Done button.
        showSkipButton(true);
        setProgressButtonEnabled(true);

        setFadeAnimation();

    }

    /**
     * this block defines the code which executes when skip
     button is pressed.
     * @param currentFragment
     */
    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        // Do something when users tap on Skip button.

        Utils.saveSharedSettingBoolean(IntroActivity.this, AppConstants.ISINTROSHOW, true);
        Intent myProfileIntent = new Intent(IntroActivity.this, HomeActivity.class);
        startActivity(myProfileIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }
//this block defines the code which executes when done
//button is pressed.
    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        Utils.saveSharedSettingBoolean(IntroActivity.this, AppConstants.ISINTROSHOW, true);
        Intent myProfileIntent = new Intent(IntroActivity.this, HomeActivity.class);
        startActivity(myProfileIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
        // Do something when users tap on Done button.
    }


}
